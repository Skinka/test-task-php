<?php

namespace Database\Seeders;

use App\Data\Seeders\DivisionSeeder;
use App\Data\Seeders\TeamsSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DivisionSeeder::class);
        $this->call(TeamsSeeder::class);
    }
}
