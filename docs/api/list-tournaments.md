# Возвращает список турниров

@apiGroup           `Tournaments`

@apiName            `list`

@api                `{GET} /api/v1/tournaments`

@apiDescription     `Возвращает список турниров`

@apiVersion         `1.0.0`

@apiSuccessExample  `{json} Success-Response`:

HTTP/1.1 200 OK

```
[
  {
    "id": 1,
    "started_at": null,
    "ended_at": null
  }
]
```
