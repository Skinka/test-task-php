# Возвращает счет команд

@apiGroup        `Tournaments`

@apiName         `getScore`

@api             `{GET} /api/v1/tournaments/{id}/score`

@apiDescription  `Возвращает счет команд`

@apiVersion      `1.0.0`

@apiSuccessExample  `{json} Success-Response`:

HTTP/1.1 200 OK

```
[
  {
    "id": 1,
    "division_id": 1,
    "team_id": 1,
    "score": 2
  }
]
```
