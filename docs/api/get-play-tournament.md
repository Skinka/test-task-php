# Возвращает игры

@apiGroup       `Tournaments`

@apiName        `toPlay`

@api            `{GET} /api/v1/tournaments/{id}/play`

@apiDescription `Возвращает игры`

@apiVersion     `1.0.0`

@apiSuccessExample  `{json} Success-Response`:

HTTP/1.1 200 OK

```
[
  {
    "id": 1,
    "division_id": 1,
    "owner_team_id": 1,
    "guest_team_id": 2,
    "owner_score": 2,
    "guest_score": 0
  }
]
```
