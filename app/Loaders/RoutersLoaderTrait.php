<?php


namespace App\Loaders;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;

/**
 * Trait RoutersLoaderTrait
 * @package App\Loaders
 */
trait RoutersLoaderTrait
{

    /**
     * Загружает роуты из UI
     */
    public function loadUiRoutes(): void
    {
        $fileRoutes = $this->getApiRoutesList();
        foreach ($fileRoutes as $file) {
            Route::group($this->getApiGroup($file), $file);
        }
        $fileRoutes = $this->getWebRoutesList();
        foreach ($fileRoutes as $file) {
            Route::group($this->getWebGroup(), $file);
        }
    }

    /**
     * Возвращает группу роутера API
     *
     * @param  string  $routeFile
     *
     * @return array
     */
    private function getApiGroup(string $routeFile): array
    {
        $fileExplode = explode('.', basename($routeFile));
        $version = $fileExplode[count($fileExplode) - 3];
        return [
            'as'         => sprintf('api.%s.', $version),
            'prefix'     => sprintf('api/%s', $version),
            'middleware' => 'api'
        ];
    }

    /**
     * Возвращает группу роутера WEB
     *
     * @return array
     */
    private function getWebGroup(): array
    {
        return [
            'middleware' => 'web'
        ];
    }

    /**
     * Возвращает список файлов с роутами API
     *
     * @return array
     */
    private function getApiRoutesList(): array
    {
        $routesPath = app_path('/UI/API/Routes');
        try {
            $files = File::files($routesPath);
        } catch (DirectoryNotFoundException $e) {
            return [];
        }
        $list = [];
        foreach ($files as $file) {
            $list[] = $file->getPathname();
        }
        return $list;
    }

    /**
     * Возвращает список файлов с роутами WEB
     *
     * @return array
     */
    private function getWebRoutesList(): array
    {
        $routesPath = app_path('/UI/WEB/Routes');
        try {
            $files = File::files($routesPath);
        } catch (DirectoryNotFoundException $e) {
            return [];
        }
        $list = [];
        foreach ($files as $file) {
            $list[] = $file->getPathname();
        }
        return $list;
    }
}
