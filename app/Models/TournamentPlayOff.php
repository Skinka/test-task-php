<?php

namespace App\Models;

use App\Data\Factories\TournamentPlayOffFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Игра в play-off
 *
 * Class TournamentPlayOff
 * @package App\Models
 * @property int        $id
 * @property int        $tournament_id
 * @property int        $tour
 * @property int        $team_1_id
 * @property int        $team_2_id
 * @property int        $team_1_score
 * @property int        $team_2_score
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 *
 * @property Tournament $tournament
 * @property Team       $team1
 * @property Team       $team2
 */
class TournamentPlayOff extends Model
{
    use HasFactory;

    protected $table    = 'tournament_play_off';
    protected $fillable = [
        'tournament_id', 'tour', 'team_1_id', 'team_2_id', 'team_1_score', 'team_2_score',
    ];

    /**
     * @return TournamentPlayOffFactory
     */
    protected static function newFactory(): TournamentPlayOffFactory
    {
        return TournamentPlayOffFactory::new();
    }

    /**
     * К какому турниру относится
     *
     * @return BelongsTo
     */
    public function tournament(): BelongsTo
    {
        return $this->belongsTo(Tournament::class);
    }

    /**
     * Команда номер 1
     *
     * @return BelongsTo
     */
    public function team1(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'team_1_id', 'id');
    }

    /**
     * Команда номер 2
     *
     * @return BelongsTo
     */
    public function team2(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'team_2_id', 'id');
    }
}
