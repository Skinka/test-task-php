<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Таблица связей турнира и принадлежностей команд к дивизионам
 *
 * Class TournamentDivisionTeam
 * @package App\Models
 *
 * @property int        $tournament_id
 * @property int        $division_id
 * @property int        $team_id
 *
 * @property Tournament $tournament
 * @property Division   $division
 * @property Team       $team
 */
class TournamentDivisionTeam extends Model
{
    use HasFactory;

    public $incrementing = false;
    public $timestamps   = false;

    protected $primaryKey = ['tournament_id', 'division_id', 'team_id'];
    protected $table      = 'tournament_division_team';
    protected $fillable   = ['tournament_id', 'division_id', 'team_id'];

    /**
     * Отношение к команде
     *
     * @return BelongsTo
     */
    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Отношение к дивизиону
     *
     * @return BelongsTo
     */
    public function division(): BelongsTo
    {
        return $this->belongsTo(Division::class);
    }

    /**
     * Отношение к турниру
     *
     * @return BelongsTo
     */
    public function tournament(): BelongsTo
    {
        return $this->belongsTo(Tournament::class);
    }
}
