<?php

namespace App\Models;

use App\Data\Factories\TeamFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Модель команды
 *
 * Class Team
 * @package App\Models
 *
 * @property int    $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Team extends Model
{
    use HasFactory;

    protected $table    = 'teams';
    protected $fillable = ['name'];

    /**
     * @return TeamFactory
     */
    protected static function newFactory(): TeamFactory
    {
        return TeamFactory::new();
    }

    /**
     * Где участвует
     *
     * @return HasMany
     */
    public function relations(): HasMany
    {
        return $this->hasMany(TournamentDivisionTeam::class);
    }
}
