<?php

namespace App\Models;

use App\Data\Factories\TournamentGameFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Модель игры в турнире
 *
 * Class TournamentGame
 * @package App\Models
 *
 * @property int        $id
 * @property int        $tournament_id
 * @property int        $division_id
 * @property int        $owner_team_id
 * @property int        $guest_team_id
 * @property int        $owner_score
 * @property int        $guest_score
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 *
 * @property Tournament $tournament
 * @property Division   $division
 * @property Team       $ownerTeam
 * @property Team       $guestTeam
 */
class TournamentGame extends Model
{
    use HasFactory;

    protected $table    = 'tournament_games';
    protected $fillable = [
        'tournament_id', 'division_id', 'owner_team_id', 'guest_team_id',
        'owner_score', 'guest_score',
    ];

    /**
     * @return TournamentGameFactory
     */
    protected static function newFactory(): TournamentGameFactory
    {
        return TournamentGameFactory::new();
    }

    /**
     * К какому турниру относится
     *
     * @return BelongsTo
     */
    public function tournament(): BelongsTo
    {
        return $this->belongsTo(Tournament::class);
    }

    /**
     * В каком дивизионе
     *
     * @return BelongsTo
     */
    public function division(): BelongsTo
    {
        return $this->belongsTo(Division::class);
    }

    /**
     * Принимающая команда
     *
     * @return BelongsTo
     */
    public function ownerTeam(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'owner_team_id', 'id');
    }

    /**
     * Команда в гостях
     *
     * @return BelongsTo
     */
    public function guestTeam(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'guest_team_id', 'id');
    }
}
