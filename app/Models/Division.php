<?php

namespace App\Models;

use App\Data\Factories\DivisionFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Модель дивизиона
 *
 * Class Division
 * @package App\Models
 *
 * @property int    $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Division extends Model
{
    use HasFactory;

    protected $table    = 'divisions';
    protected $fillable = ['name'];

    /**
     * @return DivisionFactory
     */
    protected static function newFactory(): DivisionFactory
    {
        return DivisionFactory::new();
    }

    /**
     * К каким турнирам и командам относится
     *
     * @return HasMany
     */
    public function relations(): HasMany
    {
        return $this->hasMany(TournamentDivisionTeam::class);
    }
}
