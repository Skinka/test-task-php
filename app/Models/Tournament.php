<?php

namespace App\Models;

use App\Data\Factories\TournamentFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Модель турнира
 *
 * Class Tournament
 * @package App\Models
 *
 * @property int                                 $id
 * @property Carbon                              $started_at
 * @property Carbon                              $ended_at
 * @property Carbon                              $created_at
 * @property Carbon                              $updated_at
 *
 * @property TournamentDivisionTeam[]|Collection $relations
 * @property TournamentGame[]|Collection         $games
 * @property TournamentPlayOff[]|Collection      $playOff
 */
class Tournament extends Model
{
    use HasFactory;

    protected $table    = 'tournaments';
    protected $fillable = ['started_at', 'ended_at'];
    protected $dates    = ['started_at', 'ended_at'];

    /**
     * @return TournamentFactory
     */
    protected static function newFactory(): TournamentFactory
    {
        return TournamentFactory::new();
    }

    /**
     * Какие дивизионы и команды участвуют
     *
     * @return HasMany
     */
    public function relations(): HasMany
    {
        return $this->hasMany(TournamentDivisionTeam::class);
    }

    /**
     * Сыгранные игры турнира
     *
     * @return HasMany
     */
    public function games(): HasMany
    {
        return $this->hasMany(TournamentGame::class);
    }

    /**
     * Сыгранные игры PlayOff
     *
     * @return HasMany
     */
    public function playOff(): HasMany
    {
        return $this->hasMany(TournamentPlayOff::class);
    }
}
