<?php


namespace App\Services;


use App\Data\Repositories\DivisionRepository;
use App\Data\Repositories\TeamRepository;
use App\DTO\DrawingResultDTO;
use App\Exceptions\DrawingFailedException;
use App\Interfaces\DrawerInterface;
use App\Models\Division;
use App\Models\Team;
use Illuminate\Database\Eloquent\Collection;

/**
 * Сервис жеребьевки команд
 *
 * Class Drawer
 * @package App\Services
 */
class DrawerService implements DrawerInterface
{
    private DivisionRepository $divisionRepo;
    private TeamRepository     $teamRepo;

    public function __construct(DivisionRepository $divisionRepo, TeamRepository $teamRepo)
    {
        $this->divisionRepo = $divisionRepo;
        $this->teamRepo = $teamRepo;
    }

    /**
     * @inheritDoc
     * @throws DrawingFailedException
     */
    public function drawing(): array
    {
        $divisions = $this->getDivisions();
        if ($divisions->isEmpty()) {
            throw new DrawingFailedException('Отсутствуют дивизионы');
        }
        $teams = $this->getTeams();
        if ($teams->count() < $divisions->count() * 2) {
            throw new DrawingFailedException('Не достаточное количество команд для формирования дивизионов');
        }
        $result = [];

        $splitTeams = $teams->split($divisions->count());
        foreach ($splitTeams as $key => $items) {
            $division = $divisions->get($key);
            $items->each(function ($team) use (&$result, $division) {
                $result[] = new DrawingResultDTO([
                    'division_id' => $division->id,
                    'team_id'     => $team->id
                ]);
            });
        }
        return $result;
    }

    /**
     * Возвращает список команд участвующих в турнире
     * Перемешиваются чтобы турниры отличались
     *
     * @return Team[]|Collection
     */
    protected function getTeams()
    {
        return $this->teamRepo->listShuffle();
    }

    /**
     * Возвращает список дивизионов
     *
     * @return Division[]|Collection
     */
    protected function getDivisions()
    {
        return $this->divisionRepo->list();
    }
}
