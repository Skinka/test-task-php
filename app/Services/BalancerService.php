<?php


namespace App\Services;


use App\DTO\ResultScoreDTO;
use App\Interfaces\GetScoreInterface;

class BalancerService implements GetScoreInterface
{
    const MIN_SCORE = 0;
    const MAX_SCORE = 10;

    /**
     * @inheritdoc
     *
     * @return ResultScoreDTO
     */
    public function getScore(): ResultScoreDTO
    {
        return new ResultScoreDTO([
            'team_1_score' => rand(self::MIN_SCORE, self::MAX_SCORE),
            'team_2_score' => rand(self::MIN_SCORE, self::MAX_SCORE),
        ]);
    }
}
