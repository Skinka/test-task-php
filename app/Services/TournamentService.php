<?php


namespace App\Services;


use App\Data\Repositories\TournamentRepository;
use App\DTO\DrawingResultDTO;
use App\DTO\GameDTO;
use App\DTO\PlayOffDTO;
use App\DTO\TeamScoreDTO;
use App\Exceptions\CreateFailedException;
use App\Exceptions\StartFailedException;
use App\Interfaces\GetScoreInterface;
use App\Models\Tournament;
use App\Models\TournamentDivisionTeam;
use App\Models\TournamentGame;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Сервис для работы с турниром
 *
 * Class TournamentService
 * @package App\Services
 */
class TournamentService
{
    /** @var int Максимально количество команд которое может выйти в плейофф из дивизиона */
    const MAX_PLAY_OFF_TEAM = 4;

    private TournamentRepository $repo;

    /**
     * TournamentService constructor.
     *
     * @param  TournamentRepository  $repo
     */
    public function __construct(TournamentRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Создает турнир
     *
     * @param  array|DrawingResultDTO[]  $drawing
     *
     * @return Tournament|Builder
     * @throws CreateFailedException
     */
    public function create(array $drawing)
    {
        DB::beginTransaction();
        try {
            $tournament = $this->repo->create();
            foreach ($drawing as $item) {
                $tournament->relations()->create($item->toArray());
            }
        } catch (Exception $e) {
            DB::rollBack();
            throw new CreateFailedException('Ошибка создания турнира');
        }
        DB::commit();
        return $tournament;
    }

    /**
     * Разыгрывает игры турнира
     *
     * @param  int                $tournament_id
     * @param  GetScoreInterface  $balancer
     *
     * @return Tournament
     */
    public function toPlay(int $tournament_id, GetScoreInterface $balancer): Tournament
    {
        $tournament = $this->repo->getById($tournament_id, true);

        if (!$tournament) {
            throw new StartFailedException('Турнир не найден');
        }

        if ($tournament->started_at) {
            throw new StartFailedException('Турнир уже начат');
        }
        $rel = $tournament->relations->groupBy('division_id');

        DB::beginTransaction();
        try {
            foreach ($rel as $division_id => $teams) {
                foreach ($teams as $ownerTeam) {
                    foreach ($teams as $guestTeam) {
                        // команда не может играть сама с собой
                        if ($ownerTeam->team_id == $guestTeam->team_id) {
                            continue;
                        }
                        $score = $balancer->getScore();
                        $dto = new GameDTO([
                            'division_id'   => $division_id,
                            'owner_team_id' => (int) $ownerTeam->team_id,
                            'guest_team_id' => (int) $guestTeam->team_id,
                            'owner_score'   => $score->team_1_score,
                            'guest_score'   => $score->team_2_score,
                        ]);
                        $this->playGame($tournament, $dto);
                    }
                }
            }
            $tournament->started_at = now();
            $this->repo->start($tournament_id, $tournament->started_at);
        } catch (Exception $e) {
            DB::rollBack();
            throw new StartFailedException('Ошибка начала турнира');
        }
        DB::commit();
        return $tournament;
    }

    /**
     * Возвращает счет команд в турнире
     *
     * @param  int  $tournament_id
     *
     * @return TournamentDivisionTeam[]
     */
    public function getTeamsScore(int $tournament_id): array
    {
        $tournament = $this->repo->getById($tournament_id, true);
        $games = $tournament->games;
        return $tournament->relations
            ->map(function (TournamentDivisionTeam $team) use ($games) {
                $score = $games->filter(function (TournamentGame $game) use ($team) {
                    $ownerWon = $game->owner_team_id == $team->team_id && $game->owner_score > $game->guest_score;
                    $guestWon = $game->guest_team_id == $team->team_id && $game->owner_score < $game->guest_score;
                    return $ownerWon || $guestWon;
                })->count();

                return new TeamScoreDTO([
                    'division_id' => $team->division_id,
                    'team_id'     => $team->team_id,
                    'score'       => $score
                ]);
            })->toArray();
    }

    /**
     * Проводит play-off
     *
     * @param  int              $tournament_id
     * @param  BalancerService  $balancer
     *
     * @return mixed
     */
    public function toPlayOff(int $tournament_id, BalancerService $balancer)
    {
        $tournament = $this->repo->getById($tournament_id);

        if (!$tournament) {
            throw new StartFailedException('Турнир не найден');
        }

        if ($tournament->ended_at) {
            throw new StartFailedException('Турнир уже окончен');
        }

        DB::beginTransaction();
        try {
            $divisions = collect($this->getTeamsScore($tournament_id))
                ->groupBy('division_id');
            $minCountTeams = $divisions->max(function (\Illuminate\Support\Collection $teams) {
                return $teams->count();
            });


            $maxWonByGroup = $this->calculateWonCount($minCountTeams);

            $normalizeData = $divisions
                ->map(function (\Illuminate\Support\Collection $teams, $key) use ($maxWonByGroup) {
                    return $teams->sortByDesc('score')
                        ->take($maxWonByGroup)
                        ->sortBy('score', SORT_REGULAR, $key % 2)
                        ->map(function (TeamScoreDTO $team) {
                            return $team->team_id;
                        })
                        ->toArray();
                })->values();

            $games = collect($normalizeData[0])->combine($normalizeData[1])->toArray();

            $this->playTour($tournament, $balancer, $games, 1);

            $tournament->ended_at = now();
            $this->repo->end($tournament_id, $tournament->ended_at);
        } catch (Exception $e) {
            DB::rollBack();
            throw new StartFailedException('Ошибка окончания турнира'.$e->getMessage());
        }
        DB::commit();
        return $tournament;
    }

    private function playTour(Tournament &$tournament, GetScoreInterface $balancer, array $games, $tour)
    {
        $won = collect();
        foreach ($games as $team1 => $team2) {
            $score = $balancer->getScore();
            $dto = new PlayOffDTO([
                'tour'         => $tour,
                'team_1_id'    => $team1,
                'team_2_id'    => $team2,
                'team_1_score' => $score->team_1_score,
                'team_2_score' => $score->team_2_score,
            ]);
            $this->playPlayOff($tournament, $dto);
            $won->push($dto->getWon());
        }
        if ($won->count() > 1) {
            $wonGroup = $won->split(2);
            $games = collect($wonGroup[0])->combine($wonGroup[1])->toArray();
            $this->playTour($tournament, $balancer, $games, ++$tour);
        }
    }

    /**
     * Играет игру в дивизионе
     *
     * @param  Tournament  $tournament
     * @param  GameDTO     $dto
     */
    private function playGame(Tournament $tournament, GameDTO $dto)
    {
        $tournament->games()->create($dto->toArray());
    }

    /**
     * Играет игру в play-off
     *
     * @param  Tournament  $tournament
     * @param  PlayOffDTO  $dto
     */
    private function playPlayOff(Tournament $tournament, PlayOffDTO $dto)
    {
        $tournament->playOff()->create($dto->toArray());
    }

    private function calculateWonCount(int $divisionCount): int
    {
        $val = $divisionCount > self::MAX_PLAY_OFF_TEAM ? self::MAX_PLAY_OFF_TEAM : $divisionCount;
        return $val > 1 && $val % 2 > 0 ? $val - 1 : $val;
    }
}
