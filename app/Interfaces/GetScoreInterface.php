<?php


namespace App\Interfaces;


use App\DTO\ResultScoreDTO;

interface GetScoreInterface
{
    /**
     * Возвращает очки игры
     *
     * @return ResultScoreDTO
     */
    public function getScore(): ResultScoreDTO;
}
