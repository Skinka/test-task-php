<?php


namespace App\Interfaces;

use App\DTO\DrawingResultDTO;

/**
 * Интерфейс жеребьевщика
 *
 * Interface DrawerInterface
 * @package App\Interfaces
 */
interface DrawerInterface
{
    /**
     * Расставляет команды участвующие в турните по дивизионам
     *
     * @return array|DrawingResultDTO[]
     */
    public function drawing(): array;
}
