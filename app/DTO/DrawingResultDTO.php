<?php


namespace App\DTO;


class DrawingResultDTO extends BaseDTO
{
    /** @var int Идентификатор дивизиона */
    public int $division_id;

    /** @var int Идентификатор команды */
    public int $team_id;
}
