<?php


namespace App\DTO;


/**
 * Игра
 *
 * Class PlayOffDTO
 * @package App\DTO
 */
class PlayOffDTO extends BaseDTO
{
    public int $tour;
    public int $team_1_id;
    public int $team_2_id;
    public int $team_1_score;
    public int $team_2_score;

    /**
     * Возвращает идентификатор победителя
     *
     * @return int
     */
    public function getWon(): int
    {
        return $this->team_1_score >= $this->team_2_score ? $this->team_1_id : $this->team_2_id;
    }
}
