<?php


namespace App\DTO;


class TeamScoreDTO extends BaseDTO
{
    public int $division_id;
    public int $team_id;
    public int $score;
}
