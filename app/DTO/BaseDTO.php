<?php


namespace App\DTO;


use Spatie\DataTransferObject\DataTransferObject;

/**
 * Абстрактный класс объектных данных
 *
 * Class BaseDTO
 * @package App\DTO
 */
abstract class BaseDTO extends DataTransferObject
{

}
