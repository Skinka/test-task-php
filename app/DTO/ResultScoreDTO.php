<?php


namespace App\DTO;


/**
 * Результаты очков игры
 *
 * Class ResultScoreDTO
 * @package App\DTO
 */
class ResultScoreDTO extends BaseDTO
{
    public int $team_1_score;
    public int $team_2_score;
}
