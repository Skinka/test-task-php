<?php


namespace App\DTO;


/**
 * Игра
 *
 * Class GameDTO
 * @package App\DTO
 */
class GameDTO extends BaseDTO
{
    public int $division_id;
    public int $owner_team_id;
    public int $guest_team_id;
    public int $owner_score;
    public int $guest_score;
}
