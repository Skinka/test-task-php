<?php


namespace App\UI\API\Resources;


use App\Models\Team;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TournamentResource
 * @package App\UI\API\Resources
 *
 * @mixin Team
 */
class TeamResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
        ];
    }
}
