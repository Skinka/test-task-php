<?php


namespace App\UI\API\Resources;


use App\Models\Tournament;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TournamentForListResource
 * @package App\UI\API\Resources
 *
 * @mixin Tournament
 */
class TournamentForListResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'         => $this->id,
            'started_at' => $this->started_at,
            'ended_at'   => $this->ended_at,
        ];
    }
}
