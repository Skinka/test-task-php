<?php


namespace App\UI\API\Resources;


use App\Models\Tournament;
use App\Models\TournamentDivisionTeam;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

/**
 * Class TournamentResource
 * @package App\UI\API\Resources
 *
 * @mixin Tournament
 */
class TournamentResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'         => $this->id,
            'started_at' => $this->started_at,
            'ended_at'   => $this->ended_at,
            'divisions'  => $this->processRelations()
        ];
    }

    /**
     * Формирует массив дивизионов и входящих в них команд
     *
     * @return \Illuminate\Database\Eloquent\Collection|Collection
     */
    protected function processRelations()
    {
        // выбираем записи прикрепленных дивизионов и команд к турниру
        return $this->relations
            ->groupBy('division_id') // группируем по дивизиону
            ->sortKeys() // сортируем дивизионы чтобы были по порядку (порядок по ИД)
            ->map(function (Collection $items) {
                return array_merge(
                // берем первую запись из группы для данных о дивизионе, т.к. во всех дивизион одинаковый
                    (new DivisionResource($items->first()->division))->toArray(request()),
                    [
                        // мапаем команды и присоединяем к дивизиону
                        'teams' => $items->map(function (TournamentDivisionTeam $item) {
                            return new TeamResource($item->team);
                        })->toArray()
                    ]
                );
            })
            ->values(); // сбрасываем ключи чтобы их не было в джесоне
    }
}
