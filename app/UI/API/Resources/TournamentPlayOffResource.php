<?php


namespace App\UI\API\Resources;


use App\Models\TournamentPlayOff;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TournamentGameResource
 * @package App\UI\API\Resources
 *
 * @mixin TournamentPlayOff
 */
class TournamentPlayOffResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'           => $this->id,
            'tour'         => $this->tour,
            'team_1_id'    => $this->team_1_id,
            'team_1_name'  => $this->team1->name,
            'team_2_id'    => $this->team_2_id,
            'team_2_name'  => $this->team2->name,
            'team_1_score' => $this->team_1_score,
            'team_2_score' => $this->team_2_score,
        ];
    }
}
