<?php


namespace App\UI\API\Resources;


use App\Models\TournamentGame;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TournamentGameResource
 * @package App\UI\API\Resources
 *
 * @mixin TournamentGame
 */
class TournamentGameResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'            => $this->id,
            'division_id'   => (int) $this->division_id,
            'owner_team_id' => (int) $this->owner_team_id,
            'guest_team_id' => (int) $this->guest_team_id,
            'owner_score'   => (int) $this->owner_score,
            'guest_score'   => (int) $this->guest_score,
        ];
    }
}
