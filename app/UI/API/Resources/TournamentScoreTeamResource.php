<?php


namespace App\UI\API\Resources;


use App\DTO\TeamScoreDTO;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TournamentScoreTeamResource
 * @package App\UI\API\Resources
 *
 * @mixin TeamScoreDTO
 */
class TournamentScoreTeamResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'division_id' => $this->division_id,
            'team_id'     => $this->team_id,
            'score'       => $this->score,
        ];
    }
}
