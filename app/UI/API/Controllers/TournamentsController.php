<?php


namespace App\UI\API\Controllers;


use App\Data\Repositories\TournamentRepository;
use App\Exceptions\CreateFailedException;
use App\Interfaces\DrawerInterface;
use App\Services\BalancerService;
use App\Services\TournamentService;
use App\UI\API\Resources\TournamentForListResource;
use App\UI\API\Resources\TournamentGameResource;
use App\UI\API\Resources\TournamentPlayOffResource;
use App\UI\API\Resources\TournamentResource;
use App\UI\API\Resources\TournamentScoreTeamResource;
use Illuminate\Http\JsonResponse;

/**
 * Турниры
 *
 * Class TournamentsController
 * @package App\UI\API\Controllers
 */
class TournamentsController extends ApiController
{
    /**
     * Создание турнира
     *
     * @param  TournamentService  $service
     * @param  DrawerInterface    $drawer
     *
     * @return JsonResponse
     * @throws CreateFailedException
     */
    public function create(TournamentService $service, DrawerInterface $drawer): JsonResponse
    {
        $drawing = $drawer->drawing();
        $tournament = $service->create($drawing);
        return response()->json(new TournamentResource($tournament));
    }

    /**
     * Возвращает список турниров
     *
     * @param  TournamentRepository  $repo
     *
     * @return JsonResponse
     */
    public function list(TournamentRepository $repo): JsonResponse
    {
        $tournaments = $repo->list();
        return response()->json(TournamentForListResource::collection($tournaments));
    }

    /**
     * Возвращает данные турнира
     *
     * @param  TournamentRepository  $repo
     * @param  int                   $id
     *
     * @return JsonResponse
     */
    public function get(TournamentRepository $repo, int $id): JsonResponse
    {
        $tournament = $repo->getById($id, true);
        return response()->json(new TournamentResource($tournament));
    }

    /**
     * Разыгрывает матчи турнира
     *
     * @param  TournamentService  $service
     * @param  BalancerService    $balancer
     * @param  int                $id
     *
     * @return JsonResponse
     */
    public function toPlay(TournamentService $service, BalancerService $balancer, int $id): JsonResponse
    {
        $tournament = $service->toPlay($id, $balancer);
        return response()->json(TournamentGameResource::collection($tournament->games));
    }

    /**
     * Возвращает матчи турнира
     *
     * @param  TournamentRepository  $repo
     * @param  int                   $id
     *
     * @return JsonResponse
     */
    public function getPlay(TournamentRepository $repo, int $id): JsonResponse
    {
        $games = $repo->getGames($id);
        return response()->json(TournamentGameResource::collection($games));
    }

    /**
     * Возвращает очки выигранных игр командой
     *
     * @param  TournamentService  $service
     * @param  int                $id
     *
     * @return JsonResponse
     */
    public function getScoreGames(TournamentService $service, int $id): JsonResponse
    {
        $teams = $service->getTeamsScore($id);
        return response()->json(TournamentScoreTeamResource::collection($teams));
    }


    /**
     * Разыгрывает матчи PlayOff
     *
     * @param  TournamentService  $service
     * @param  BalancerService    $balancer
     * @param  int                $id
     *
     * @return JsonResponse
     */
    public function toPlayOff(TournamentService $service, BalancerService $balancer, int $id): JsonResponse
    {
        $tournament = $service->toPlayOff($id, $balancer);
        return response()->json(TournamentPlayOffResource::collection($tournament->playOff));
    }


    /**
     * Разыгрывает матчи PlayOff
     *
     * @param  TournamentRepository  $repo
     * @param  int                   $id
     *
     * @return JsonResponse
     */
    public function getPlayOff(TournamentRepository $repo, int $id): JsonResponse
    {
        $games = $repo->getPlayOffGames($id);
        return response()->json(TournamentPlayOffResource::collection($games));
    }
}
