<?php

use App\UI\API\Controllers\TournamentsController;
use Illuminate\Support\Facades\Route;

/**
 * @apiGroup           Tournaments
 * @apiName            toPlay
 *
 * @api                {POST} /api/v1/tournaments/{id}/play
 * @apiDescription     Разыгрывает игры
 *
 * @apiVersion         1.0.0
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * [
 *   {
 *     "id": 1,
 *     "division_id": 1,
 *     "owner_team_id": 1,
 *     "guest_team_id": 2,
 *     "owner_score": 2,
 *     "guest_score": 0
 *   }
 * ]
 */
Route::post('/tournaments/{id}/play', [TournamentsController::class, 'toPlay'])
    ->name('to_play_tournament');
