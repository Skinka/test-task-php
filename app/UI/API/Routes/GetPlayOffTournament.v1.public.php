<?php

use App\UI\API\Controllers\TournamentsController;
use Illuminate\Support\Facades\Route;

/**
 * @apiGroup           Tournaments
 * @apiName            toPlay
 *
 * @api                {GET} /api/v1/tournaments/{id}/play-off
 * @apiDescription     Возвращает игры play-off
 *
 * @apiVersion         1.0.0
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * [
 *   {
 *     "id": 1,
 *     "tour": 1,
 *     "team_1_id": 1,
 *     "team_1_name": "Team 1",
 *     "team_2_id": 2,
 *     "team_2_name": "Team 2",
 *     "team_1_score": 0,
 *     "team_2_score": 2,
 *   }
 * ]
 */
Route::get('/tournaments/{id}/play-off', [TournamentsController::class, 'getPlayOff'])
    ->name('get_play_off_tournament');
