<?php

use App\UI\API\Controllers\TournamentsController;
use Illuminate\Support\Facades\Route;

/**
 * @apiGroup           Tournaments
 * @apiName            list
 *
 * @api                {GET} /api/v1/tournaments
 * @apiDescription     Возвращает список турниров
 *
 * @apiVersion         1.0.0
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * [
 *   {
 *     "id": 1,
 *     "started_at": null,
 *     "ended_at": null
 *   }
 * ]
 */
Route::get('/tournaments', [TournamentsController::class, 'list'])
    ->name('list_tournaments');
