<?php

use App\UI\API\Controllers\TournamentsController;
use Illuminate\Support\Facades\Route;

/**
 * @apiGroup           Tournaments
 * @apiName            create
 *
 * @api                {POST} /api/v1/tournaments
 * @apiDescription     Создает новый турнир
 *
 * @apiVersion         1.0.0
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "id": 20,
 *   "started_at": null,
 *   "ended_at": null,
 *   "divisions": [
 *     {
 *       "id": 1,
 *       "name": "A",
 *       "teams": [
 *         {
 *           "id": 1,
 *           "name": "Blanditiis North Kailyn"
 *         }
 *       ]
 *     }
 *   ]
 * }
 */
Route::post('/tournaments', [TournamentsController::class, 'create'])
    ->name('create_tournament');
