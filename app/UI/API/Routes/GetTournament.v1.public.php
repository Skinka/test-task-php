<?php

use App\UI\API\Controllers\TournamentsController;
use Illuminate\Support\Facades\Route;

/**
 * @apiGroup           Tournaments
 * @apiName            get
 *
 * @api                {GET} /api/v1/tournaments/{id}
 * @apiDescription     Возвращает данные о турнире
 *
 * @apiVersion         1.0.0
 *
 * @apiParameter       {int}  id  Идентификатор турнира
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "id": 1,
 *   "started_at": null,
 *   "ended_at": null,
 *   "divisions": [
 *     {
 *       "id": 1,
 *       "name": "A",
 *       "teams": [
 *         {
 *           "id": 2,
 *           "name": "Quo West Richieborough"
 *         }
 *       ]
 *     }
 *   ]
 * }
 */
Route::get('/tournaments/{id}', [TournamentsController::class, 'get'])
    ->name('tournament');
