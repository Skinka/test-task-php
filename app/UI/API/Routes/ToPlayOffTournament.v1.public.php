<?php

use App\UI\API\Controllers\TournamentsController;
use Illuminate\Support\Facades\Route;

/**
 * @apiGroup           Tournaments
 * @apiName            toPlay
 *
 * @api                {POST} /api/v1/tournaments/{id}/play-off
 * @apiDescription     Разыгрывает игры play-off
 *
 * @apiVersion         1.0.0
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * [
 *   {
 *     "id": 1,
 *     "tour": 1,
 *     "team_1_id": 1,
 *     "team_2_id": 2,
 *     "team_1_score": 2,
 *     "team_2_score": 0
 *   }
 * ]
 */
Route::post('/tournaments/{id}/play-off', [TournamentsController::class, 'toPlayOff'])
    ->name('to_play_off_tournament');
