<?php


namespace App\UI\WEB\Controllers;

use App\Data\Repositories\TournamentRepository;
use Illuminate\Contracts\View\View;

/**
 * Контроллер страниц фронтенда
 *
 * Class SiteController
 * @package App\Http\Controllers
 */
class SiteController extends WebController
{
    /**
     * Основная страница турниров
     *
     * @param  TournamentRepository  $tournamentRepo
     *
     * @return View
     */
    public function main(TournamentRepository $tournamentRepo): View
    {
        $tournaments = $tournamentRepo->list();
        return view('APP::site.main', ['tournaments' => $tournaments]);
    }
}
