<?php

use App\UI\WEB\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

/**
 * Главная страница приложения
 */
Route::get('/', [SiteController::class, 'main'])->name('main');
