<?php
/**
 * Шаблон главной страницы
 * @var \App\Models\Tournament[] $tournaments
 */
?>
@extends('APP::layouts.main')

@section('title', 'Турниры')

@section('content')

    <div class="container-fluid">
        <h1>Турниры</h1>
        <div id="errors"></div>
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <ul class="list-unstyled">
                        <li>
                            <button id="new-tournament" class="btn btn-success btn-block">Новый турнир</button>
                        </li>
                    </ul>
                    <ul id="tournaments" class="list-unstyled"></ul>
                </div>
            </div>
            <div class="col-md-9">
                <div id="title"></div>
                <div id="content"></div>
                <div id="actions" class="mt-3"></div>
                <div id="play-off" class="mt-3"></div>
            </div>
        </div>
    </div>
@endsection
