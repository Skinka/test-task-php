<?php

namespace App\Data\Seeders;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Division::factory()
            ->count(2)
            ->state(new Sequence(
                ['name' => 'A'],
                ['name' => 'B'],
            ))
            ->create();
    }
}
