<?php


namespace App\Data\Repositories;


use App\Models\Team as Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Репозиторий для команд
 *
 * Class TeamRepository
 * @package App\Data\Repositories
 */
class TeamRepository extends BaseRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * Возвращает список команд в произвольной сортировке
     *
     * @return Collection|Model[]
     */
    public function listShuffle(): Collection
    {
        return $this->startConditions()->get()->shuffle();
    }
}
