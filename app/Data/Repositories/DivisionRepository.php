<?php


namespace App\Data\Repositories;


use App\Models\Division as Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Репозиторий для дивизионов
 *
 * Class DivisionRepository
 * @package App\Data\Repositories
 */
class DivisionRepository extends BaseRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * Возвращает список дивизионов
     *
     * @return Collection|Model[]
     */
    public function list(): Collection
    {
        return $this->startConditions()->get();
    }
}
