<?php


namespace App\Data\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 * @package App\Data\Repositories
 */
abstract class BaseRepository
{
    /** @var Model */
    protected $model;

    /**
     * CoreRepository constructor.
     */
    public function __construct()
    {
        $this->model = app($this->getModelClass());
    }


    /**
     * Return model class name
     *
     * @return string
     */
    abstract protected function getModelClass(): string;


    /**
     * Start query from model
     *
     * @return Model|Builder
     */
    protected function startConditions()
    {
        return clone $this->model;
    }
}
