<?php


namespace App\Data\Repositories;


use App\Models\Tournament as Model;
use App\Models\TournamentGame;
use App\Models\TournamentPlayOff;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Репозиторий для турниров
 *
 * Class TournamentRepository
 * @package App\Data\Repositories
 */
class TournamentRepository extends BaseRepository
{
    /**
     * Возвращает турнир по идентификатору
     *
     * @param  int   $id
     * @param  bool  $with
     *
     * @return Builder|Model|null
     */
    public function getById(int $id, $with = false): ?Model
    {
        $query = $this->startConditions()->where('id', $id);
        if ($with) {
            $query->with([
                'relations' => function ($builder) {
                    $builder->with(['team', 'division']);
                }
            ]);
        }
        return $query->first();
    }

    /**
     * Возвращает игры турнира
     *
     * @param  int  $id
     *
     * @return Collection|TournamentGame[]
     */
    public function getGames(int $id): Collection
    {
        /** @var Model $tournament */
        $tournament = $this->startConditions()->where('id', $id)->first();
        return $tournament->games()->orderBy('division_id')->get();
    }

    /**
     * Возвращает play-off игры турнира
     *
     * @param  int  $id
     *
     * @return Collection|TournamentPlayOff[]
     */
    public function getPlayOffGames(int $id): Collection
    {
        /** @var Model $tournament */
        $tournament = $this->startConditions()->where('id', $id)->first();
        return $tournament->playOff()->orderBy('tour')->get();
    }

    /**
     * Возвращает список турниров
     *
     * @return Collection|Model[]
     */
    public function list(): Collection
    {
        return $this->startConditions()->orderBy('created_at')->get();
    }

    /**
     * Создает турнир
     *
     * @return Model|Builder
     */
    public function create(): Model
    {
        return $this->startConditions()->create();
    }

    /**
     * Устанавливает признак что игры сыграны
     *
     * @param  int     $id
     * @param  Carbon  $time
     *
     * @return bool
     */
    public function start(int $id, Carbon $time): bool
    {
        return $this->startConditions()->where('id', $id)->update(['started_at' => $time]);
    }

    /**
     * Устанавливает признак что турнир окончен
     *
     * @param  int     $id
     * @param  Carbon  $time
     *
     * @return bool
     */
    public function end(int $id, Carbon $time): bool
    {
        return $this->startConditions()->where('id', $id)->update(['ended_at' => $time]);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
