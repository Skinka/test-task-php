<?php

namespace App\Data\Factories;

use App\Models\TournamentPlayOff;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class TournamentPlayOffFactory
 * @package Database\Factories
 */
class TournamentPlayOffFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TournamentPlayOff::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
