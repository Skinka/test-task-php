<?php

namespace App\Data\Factories;

use App\Models\TournamentGame;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class TournamentGameFactory
 * @package Database\Factories
 */
class TournamentGameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TournamentGame::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
