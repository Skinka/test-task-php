<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentDivisionTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_division_team', function (Blueprint $table) {
            $table->foreignId('tournament_id')->constrained();
            $table->foreignId('division_id')->constrained();
            $table->foreignId('team_id')->constrained();

            $table->primary([
                'tournament_id', 'division_id', 'team_id'
            ], 'tournament_division_team_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament_division_team', function (Blueprint $table) {
            $table->dropForeign(['tournament_id']);
            $table->dropForeign(['division_id']);
            $table->dropForeign(['team_id']);
            $table->dropPrimary('tournament_division_team_primary');
        });
        Schema::dropIfExists('tournament_division_team');
    }
}
