<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentPlayOffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_play_off', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tournament_id')->constrained();
            $table->unsignedInteger('tour');
            $table->foreignId('team_1_id')->constrained('teams');
            $table->foreignId('team_2_id')->constrained('teams');
            $table->unsignedInteger('team_1_score');
            $table->unsignedInteger('team_2_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament_play_off', function (Blueprint $table) {
            $table->dropForeign(['tournament_id']);
            $table->dropForeign(['team_1_id']);
            $table->dropForeign(['team_2_id']);
        });
        Schema::dropIfExists('tournament_play_off');
    }
}
