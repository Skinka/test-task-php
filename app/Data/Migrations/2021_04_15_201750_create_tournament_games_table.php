<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_games', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tournament_id')->constrained();
            $table->foreignId('division_id')->constrained();
            $table->foreignId('owner_team_id')->constrained('teams');
            $table->foreignId('guest_team_id')->constrained('teams');
            $table->unsignedInteger('owner_score');
            $table->unsignedInteger('guest_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament_games', function (Blueprint $table) {
            $table->dropForeign(['tournament_id']);
            $table->dropForeign(['division_id']);
            $table->dropForeign(['owner_team_id']);
            $table->dropForeign(['guest_team_id']);
        });
        Schema::dropIfExists('tournament_games');
    }
}
