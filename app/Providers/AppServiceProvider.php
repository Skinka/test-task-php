<?php

namespace App\Providers;

use App\Interfaces\DrawerInterface;
use App\Loaders\RoutersLoaderTrait;
use App\Services\DrawerService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    use RoutersLoaderTrait;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DrawerInterface::class, DrawerService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(app_path('/Data/Migrations'));
        $this->loadViewsFrom(app_path('/UI/WEB/Views'), 'APP');
        $this->loadUiRoutes();
    }
}
