<?php

namespace Tests\Unit\Services;

use App\Data\Repositories\TournamentRepository;
use App\DTO\ResultScoreDTO;
use App\Exceptions\StartFailedException;
use App\Interfaces\GetScoreInterface;
use App\Models\Division;
use App\Models\Team;
use App\Models\Tournament;
use App\Services\BalancerService;
use App\Services\DrawerService;
use App\Services\TournamentService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery\MockInterface;
use Tests\TestCase;

class TournamentServiceToPlayTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Игры команд в турнире
     */
    public function test_to_play()
    {

        /** @var TournamentService $tournamentService */
        $tournamentService = app(TournamentService::class);
        $data = $this->seedTournament($tournamentService);

        $first_score = 3;
        $second_score = 2;
        /** @var GetScoreInterface $mock */
        $mock = $this->mockBalancer($first_score, $second_score);

        $tournament = $tournamentService->toPlay($data->id, $mock);
        $this->assertNotNull($tournament->games);
        $this->assertNotEmpty($tournament->games);

        // количество игр. Количество команд умноженное на 2 минус количество команд
        // поскольку замокано 4 команды в 2 дивизионах, то игр будет по 4 в дивизионе. 4*2-2=2
        $this->assertCount(4, $tournament->games);
    }

    /**
     * Игры команд в турнире
     * Ошибка: Турнир не найден
     */
    public function test_to_play_not_found()
    {
        /** @var TournamentService $service */
        $service = app(TournamentService::class);

        $this->expectException(StartFailedException::class);
        $this->expectExceptionMessage('Турнир не найден');
        $service->toPlay(100500, new BalancerService());
    }

    /**
     * Игры команд в турнире
     * Ошибка: Турнир не найден
     */
    public function test_to_play_started_exception()
    {
        /** @var TournamentService $service */
        $service = app(TournamentService::class);

        /** @var TournamentService $tournamentService */
        $tournamentService = app(TournamentService::class);
        $data = $this->seedTournament($tournamentService);

        /** @var TournamentRepository $repo */
        $repo = app(TournamentRepository::class);
        $repo->start($data->id, now());

        $this->expectException(StartFailedException::class);
        $this->expectExceptionMessage('Турнир уже начат');
        $service->toPlay($data->id, new BalancerService());
    }


    /**
     * Игры команд в турнире
     * Ошибка: Турнир не найден
     */
    public function test_to_play_start_failed()
    {
        /** @var TournamentService $service */
        $service = app(TournamentService::class);

        /** @var TournamentService $tournamentService */
        $tournamentService = app(TournamentService::class);
        $data = $this->seedTournament($tournamentService);

        /** @var GetScoreInterface $balancer */
        $balancer = $this->mockBalancer(-2, 0, 1);

        $this->expectException(StartFailedException::class);
        $this->expectExceptionMessage('Ошибка начала турнира');
        $service->toPlay($data->id, $balancer);
    }

    /**
     * Создает в базе турнир с идентификатором "1" и возвращает его
     *
     * @param  TournamentService  $service
     *
     * @return Tournament
     */
    private function seedTournament(TournamentService $service): Tournament
    {
        Division::factory()->create(['id' => 1, 'name' => 'A']);
        Division::factory()->create(['id' => 2, 'name' => 'A']);
        Team::factory()->create(['id' => 1, 'name' => 'Team 1']);
        Team::factory()->create(['id' => 2, 'name' => 'Team 2']);
        Team::factory()->create(['id' => 3, 'name' => 'Team 3']);
        Team::factory()->create(['id' => 4, 'name' => 'Team 4']);
        $drawer = app(DrawerService::class);
        $drown = $drawer->drawing();

        return $service->create($drown);
    }

    /**
     * Мокает балансер, Мок основан на GetScoreInterface
     *
     * @param  int  $first_score
     * @param  int  $second_score
     *
     * @return MockInterface
     */
    private function mockBalancer(int $first_score, int $second_score, $times = 4): MockInterface
    {
        /** @var GetScoreInterface $mock */
        return $this->mock(
            GetScoreInterface::class,
            function (MockInterface $mock) use ($first_score, $second_score, $times) {
                $data = new ResultScoreDTO([
                    'team_1_score' => $first_score,
                    'team_2_score' => $second_score
                ]);
                $mock->shouldReceive('getScore')->andReturn($data)->times($times);
            }
        );
    }
}
