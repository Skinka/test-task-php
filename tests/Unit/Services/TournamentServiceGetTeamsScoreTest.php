<?php

namespace Tests\Unit\Services;

use App\Models\Division;
use App\Models\Team;
use App\Services\DrawerService;
use App\Services\TournamentService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TournamentServiceGetTeamsScoreTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Возвращает список очков побед команд
     */
    public function test_get_teams_score()
    {
        Division::factory()->create(['id' => 1, 'name' => 'A']);
        Division::factory()->create(['id' => 2, 'name' => 'A']);
        Team::factory()->create(['id' => 1, 'name' => 'Team 1']);
        Team::factory()->create(['id' => 2, 'name' => 'Team 2']);
        Team::factory()->create(['id' => 3, 'name' => 'Team 3']);
        Team::factory()->create(['id' => 4, 'name' => 'Team 4']);

        /** @var DrawerService $drawer */
        $drawer = app(DrawerService::class);
        $drown = $drawer->drawing();
        /** @var TournamentService $service */
        $service = app(TournamentService::class);
        $tournament = $service->create($drown);

        $games = $service->getTeamsScore($tournament->id);
        $this->assertCount(4, $games);
    }
}
