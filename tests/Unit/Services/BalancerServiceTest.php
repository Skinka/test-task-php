<?php

namespace Tests\Unit\Services;

use App\DTO\ResultScoreDTO;
use App\Services\BalancerService;
use Tests\TestCase;

class BalancerServiceTest extends TestCase
{

    public function test_get_score()
    {
        $balancer = app(BalancerService::class);
        $score = $balancer->getScore();
        $this->assertTrue($score instanceof ResultScoreDTO);
        $this->assertGreaterThanOrEqual(BalancerService::MIN_SCORE, $score->team_1_score);
        $this->assertLessThanOrEqual(BalancerService::MAX_SCORE, $score->team_1_score);
        $this->assertGreaterThanOrEqual(BalancerService::MIN_SCORE, $score->team_2_score);
        $this->assertLessThanOrEqual(BalancerService::MAX_SCORE, $score->team_2_score);
    }
}
