<?php

namespace Tests\Unit\Services;

use App\Data\Repositories\DivisionRepository;
use App\Data\Repositories\TeamRepository;
use App\DTO\DrawingResultDTO;
use App\Exceptions\DrawingFailedException;
use App\Models\Division;
use App\Models\Team;
use App\Services\DrawerService;
use Illuminate\Database\Eloquent\Collection;
use Mockery\MockInterface;
use Tests\TestCase;

/**
 * Class DrawerServiceTest
 * @package Services
 */
class DrawerServiceTest extends TestCase
{
    /**
     * Тестирование жеребьевка команд между дивизионами
     *
     * @return void
     * @throws DrawingFailedException
     */
    public function test_drawing(): void
    {
        /** @var DivisionRepository $mockDivision */
        $mockDivision = $this->mock(DivisionRepository::class, function (MockInterface $mock) {
            $data = new Collection([
                Division::factory()->makeOne(['id' => 1, 'name' => 'A']),
                Division::factory()->makeOne(['id' => 2, 'name' => 'B']),
            ]);
            $mock->shouldReceive('list')->andReturn($data)->once();
        });

        /** @var TeamRepository $mockTeam */
        $mockTeam = $this->mock(TeamRepository::class, function (MockInterface $mock) {
            $data = new Collection([
                Team::factory()->makeOne(['id' => 2, 'name' => 'Team 2']),
                Team::factory()->makeOne(['id' => 3, 'name' => 'Team 3']),
                Team::factory()->makeOne(['id' => 1, 'name' => 'Team 1']),
                Team::factory()->makeOne(['id' => 4, 'name' => 'Team 4']),
            ]);
            $mock->shouldReceive('listShuffle')->andReturn($data)->once();
        });

        $drawer = new DrawerService($mockDivision, $mockTeam);

        $drown = $drawer->drawing();

        $this->assertNotNull($drown);
        $this->assertNotEmpty($drown);
        $this->assertEquals([
            new DrawingResultDTO(['division_id' => 1, 'team_id' => 2]),
            new DrawingResultDTO(['division_id' => 1, 'team_id' => 3]),
            new DrawingResultDTO(['division_id' => 2, 'team_id' => 1]),
            new DrawingResultDTO(['division_id' => 2, 'team_id' => 4]),
        ], $drown);
    }

    /**
     * Тестирование жеребьевка команд между дивизионами
     * Отсутствуют дивизионы
     *
     * @return void
     * @throws DrawingFailedException
     */
    public function test_drawing_fail_no_divisions(): void
    {
        /** @var DivisionRepository $mockDivision */
        $mockDivision = $this->mock(DivisionRepository::class, function (MockInterface $mock) {
            $data = new Collection([]);
            $mock->shouldReceive('list')->andReturn($data)->once();
        });

        $drawer = new DrawerService($mockDivision, new TeamRepository());

        $this->expectException(DrawingFailedException::class);
        $this->expectExceptionMessage('Отсутствуют дивизионы');
        $drawer->drawing();
    }

    /**
     * Тестирование жеребьевка команд между дивизионами
     * Не хватает команд для заполнения дивизионов
     * Должно быть как минимум по 2 команды на каждый дивизион
     *
     * @return void
     */
    public function test_drawing_fail_no_enough_teams(): void
    {
        /** @var DivisionRepository $mockDivision */
        $mockDivision = $this->mock(DivisionRepository::class, function (MockInterface $mock) {
            $data = new Collection([
                Division::factory()->makeOne(['id' => 1, 'name' => 'A']),
                Division::factory()->makeOne(['id' => 2, 'name' => 'B']),
            ]);
            $mock->shouldReceive('list')->andReturn($data)->once();
        });

        /** @var TeamRepository $mockTeam */
        $mockTeam = $this->mock(TeamRepository::class, function (MockInterface $mock) {
            $data = new Collection([
                Team::factory()->makeOne(['id' => 1, 'name' => 'Team 1']),
                Team::factory()->makeOne(['id' => 2, 'name' => 'Team 2']),
                Team::factory()->makeOne(['id' => 3, 'name' => 'Team 3']),
            ]);
            $mock->shouldReceive('listShuffle')->andReturn($data)->once();
        });

        $drawer = new DrawerService($mockDivision, $mockTeam);

        $this->expectException(DrawingFailedException::class);
        $this->expectExceptionMessage('Не достаточное количество команд для формирования дивизионов');
        $drawer->drawing();
    }
}
