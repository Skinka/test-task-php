<?php

namespace Tests\Unit\Services;

use App\Exceptions\CreateFailedException;
use App\Exceptions\DrawingFailedException;
use App\Models\Division;
use App\Models\Team;
use App\Models\Tournament;
use App\Services\DrawerService;
use App\Services\TournamentService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TournamentServiceCreateTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Создание турнира
     */
    public function test_create()
    {
        Division::factory()->create(['id' => 1, 'name' => 'A']);
        Division::factory()->create(['id' => 2, 'name' => 'B']);
        Team::factory()->create(['id' => 2, 'name' => 'Team 2']);
        Team::factory()->create(['id' => 3, 'name' => 'Team 3']);
        Team::factory()->create(['id' => 1, 'name' => 'Team 1']);
        Team::factory()->create(['id' => 4, 'name' => 'Team 4']);

        $drawer = app(DrawerService::class);
        $drown = $drawer->drawing();

        $tournamentService = app(TournamentService::class);
        $data = $tournamentService->create($drown);

        $this->assertNotNull($data);
        $this->assertNotEmpty($data);

        /** @var Tournament $tournament */
        $tournament = Tournament::query()->first();
        $this->assertNotNull($tournament);
        $this->assertEquals($tournament->id, $data->id);
        $this->assertEquals(4, $tournament->relations()->count());
    }

    /**
     * Ошибка создания турнира
     *
     * @throws DrawingFailedException
     * @throws CreateFailedException
     */
    public function test_create_fail()
    {
        Division::factory()->create(['id' => 1, 'name' => 'A']);
        Division::factory()->create(['id' => 2, 'name' => 'B']);
        Team::factory()->create(['id' => 2, 'name' => 'Team 2']);
        Team::factory()->create(['id' => 3, 'name' => 'Team 3']);
        Team::factory()->create(['id' => 1, 'name' => 'Team 1']);
        Team::factory()->create(['id' => 4, 'name' => 'Team 4']);

        $drawer = app(DrawerService::class);
        $drown = $drawer->drawing();

        // Вызовет ошибку добавления связей
        $drown[] = collect();

        $tournamentService = app(TournamentService::class);

        $this->expectException(CreateFailedException::class);
        $this->expectExceptionMessage('Ошибка создания турнира');
        $tournamentService->create($drown);
    }
}
