<?php

namespace Tests\Feature\UI\WEB\Controllers;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MainTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_main_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
