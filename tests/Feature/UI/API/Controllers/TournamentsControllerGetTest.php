<?php

namespace Tests\Feature\UI\API\Controllers;

use App\Models\Division;
use App\Models\Team;
use App\Services\DrawerService;
use App\Services\TournamentService;
use App\UI\API\Resources\TournamentResource;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class TournamentsControllerGetTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Тест списка турниров
     */
    public function test_get()
    {
        Division::factory()->create(['id' => 1, 'name' => 'A']);
        Division::factory()->create(['id' => 2, 'name' => 'B']);
        Team::factory()->create(['id' => 2, 'name' => 'Team 2']);
        Team::factory()->create(['id' => 3, 'name' => 'Team 3']);
        Team::factory()->create(['id' => 1, 'name' => 'Team 1']);
        Team::factory()->create(['id' => 4, 'name' => 'Team 4']);

        $drawer = app(DrawerService::class);
        $drown = $drawer->drawing();

        $tournamentService = app(TournamentService::class);
        $data = $tournamentService->create($drown);

        $response = $this->getJson('/api/v1/tournaments/1');
        $response->assertOk();
        $json = new JsonResponse(new TournamentResource($data));
        $response->assertJson($json->getData(true));
    }
}
