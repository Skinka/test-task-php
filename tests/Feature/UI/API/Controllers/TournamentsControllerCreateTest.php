<?php

namespace Tests\Feature\UI\API\Controllers;

use App\Models\Tournament;
use App\UI\API\Resources\TournamentResource;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class TournamentsControllerCreateTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return void
     */
    public function test_success()
    {
        $this->seed();

        $response = $this->post('/api/v1/tournaments');
        $response->assertStatus(200);

        $data = Tournament::query()->where('id', 1)->first();
        $json = new JsonResponse(new TournamentResource($data));
        $response->assertJson($json->getData(true));
    }

    /**
     * @return void
     */
    public function test_fail()
    {
        $this->seed();
        DB::table('divisions')->delete();

        $response = $this->postJson('/api/v1/tournaments');
        $response->assertStatus(422);
    }
}
