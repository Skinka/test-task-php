<?php

namespace Tests\Feature\UI\API\Controllers;

use App\Models\Tournament;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TournamentsControllerListTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Тест списка турниров
     */
    public function test_list()
    {
        $this->seed();

        Tournament::factory()->create([
            'started_at' => null,
            'ended_at'   => null
        ]);

        $response = $this->getJson('/api/v1/tournaments');
        $response->assertOk();
        $response->assertJson([
            [
                'id'         => 1,
                'started_at' => null,
                'ended_at'   => null
            ]
        ]);

    }
}
