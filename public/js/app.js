const $error = $('#errors'),
    $content = $('#content'),
    $tournaments = $('#tournaments'),
    $actions = $('#actions'),
    $playOff = $('#play-off'),
    $title = $('#title');

$.ajaxSetup({
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

$(document).ready(() => {
    updateTournamentsList();
    bindOpenTournament();
    bindNewTournament();
    bindPlayTournament();
    bindPlayOffTournament();
});

updateTournamentsList = () => {
    $error.empty();
    $.ajax({
        url: '/api/v1/tournaments',
        method: 'GET',
    }).done((res) => {
        $tournaments.empty();
        res.forEach((item) => {
            $(`
                <li>
                    <button data-id="${item.id}" class="btn btn-primary btn-block mb-1 btn-open-tournament">
                        Турнир №${item.id}
                    </button>
                    <p>${genDateRangeString(item.started_at, item.ended_at)}</p>
                </li>
            `).appendTo($tournaments)
        });
    }).fail((jqXHR) => {
        $error.html(jqXHR.responseJSON.message);
    });
}

genDateRangeString = (start, end) => {
    let time = '';
    if (start) {
        let date = new Date(start);
        time += `Начался: ${date.toDateString()}`;
        if (end) {
            date = new Date(end);
            time += ` / Закончился: ${date.toDateString()}`;
        }
    }
    return time;
}

bindNewTournament = () => {
    $('body').on('click', '#new-tournament', (e) => {
        e.preventDefault();
        fillTournamentPage(`/api/v1/tournaments`, 'POST');
    });

}

bindPlayTournament = () => {
    $('body').on('click', '#play-tournament', (e) => {
        e.preventDefault();
        let $this = $(e.target);
        toPlay($this, $this.data('id'), 'POST', true);
    });

}

bindPlayOffTournament = () => {
    $('body').on('click', '#play-off-tournament', (e) => {
        e.preventDefault();
        toPlayOff($(e.target), e.target.dataset.id, 'POST');
    });

}

bindOpenTournament = () => {
    $('body').on('click', `.btn-open-tournament`, (e) => {
        e.preventDefault();
        fillTournamentPage(`/api/v1/tournaments/${e.target.dataset.id}`, 'GET');
    });
}

fillTournamentPage = (url, type) => {
    $error.empty();
    $actions.empty();
    $.ajax({
        url: url,
        method: type,
    }).done((res) => {
        $title.html(`
            <h2>Турнир №${res.id}</h2>
            <h3>${genDateRangeString(res.started_at, res.ended_at)}</h3>
        `);
        $content.empty();
        let html = '';
        html += `<div class="row">`;
        res.divisions.forEach((division) => {
            let teamsCol = '';
            let teamsRow = [];
            division.teams.forEach((owner) => {
                teamsCol += `<th title="${owner.name}">${owner.id}</th>`;
                let row = `<tr><th title="${owner.name}">${owner.id}</th>`;
                division.teams.forEach((guest) => {
                    if (owner.id === guest.id) {
                        row += `<td class="table-dark"></td>`;
                    } else {
                        row += `<td data-rel="rel-${division.id}-${owner.id}-${guest.id}"></td>`;
                    }
                });
                row += `<th title="Общий счет побед - ${owner.name}" data-score="score-${division.id}-${owner.id}"></th>`;
                row += `</tr>`;
                teamsRow.push(row)
            })
            html += `
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">Дивизион "${division.name}"</div>
                            <div class="card-body">
                                <table class="table table-bordered table-sm table-hover table-striped">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th title="Команды">T</th>
                                            ${teamsCol}
                                            <th title="Счет">S</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${teamsRow.join('')}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>`;
        });
        html += `</div>`;
        $content.html(html);
        if (!res.started_at) {
            $(`
                <button id="play-tournament" data-id="${res.id}" class="btn btn-success">
                    Разыграть
                </button>
            `).appendTo($actions);
        }
        toPlay($(''), res.id, 'GET', res.started_at && !res.ended_at);
        if (type === 'POST') {
            updateTournamentsList();
        }
    }).fail((jqXHR) => {
        $error.html(jqXHR.responseJSON.message);
    });
}

toPlay = ($obj, tournamentId, method, needPlayOff) => {
    $error.empty();
    $.ajax({
        url: `/api/v1/tournaments/${tournamentId}/play`,
        method: method,
    }).done((res) => {
        res.forEach((item) => {
            $(`[data-rel="rel-${item.division_id}-${item.owner_team_id}-${item.guest_team_id}"]`)
                .html(`${item.owner_score}:${item.guest_score}`)
        })
        $obj.remove();
        if (needPlayOff) {
            $(`
                <button id="play-off-tournament" data-id="${tournamentId}" class="btn btn-success">
                    Разыграть PlayOff
                </button>
            `).appendTo($actions);
        }
        getScore(tournamentId);
    }).fail((jqXHR) => {
        $error.html(jqXHR.responseJSON.message);
    });
}

getScore = (tournamentId) => {
    $.ajax({
        url: `/api/v1/tournaments/${tournamentId}/score`,
        method: 'GET',
    }).done(function (res) {
        res.forEach(function (item) {
            $(`[data-score="score-${item.division_id}-${item.team_id}"]`)
                .html(`${item.score}`)
        });
        toPlayOff($(''), tournamentId, 'GET');
    }).fail(function (jqXHR, textStatus) {
        $error.html(jqXHR.responseJSON.message);
    });
}

toPlayOff = ($obj, tournamentId, method) => {
    $error.empty();
    $playOff.empty();
    $.ajax({
        url: `/api/v1/tournaments/${tournamentId}/play-off`,
        method: method,
    }).done((res) => {
        let tour = 0;
        let html = '';
        html += `<div class="row align-items-center">`;
        res.forEach((item) => {
            if (tour !== item.tour) {
                if (tour !== 0) {
                    html += `</div>`;
                }
                html += `<div class="col">`
                tour = item.tour
            }
            html += `
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <div>${item.team_1_id} - ${item.team_1_name}</div>
                                <hr>
                                <div>${item.team_2_id} - ${item.team_2_name}</div>
                            </div>
                            <div class="col-4 text-center">
                                <strong>${item.team_1_score}:${item.team_2_score}</strong>
                            </div>
                        </div>
                    </div>
                </div>`
        })
        html += `</div></div>`;
        $playOff.html(html);
        $obj.remove();
    }).fail((jqXHR) => {
        $error.html(jqXHR.responseJSON.message);
    });
}
